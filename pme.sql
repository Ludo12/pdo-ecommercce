-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : ven. 26 nov. 2021 à 17:47
-- Version du serveur :  8.0.27-0ubuntu0.20.04.1
-- Version de PHP : 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pme`
--
CREATE DATABASE IF NOT EXISTS `pme` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `pme`;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id_article` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `designation` varchar(100) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `prix` decimal(8,2) NOT NULL,
  `id_categorie` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`id_article`, `designation`, `image`, `prix`, `id_categorie`) VALUES
('CA300', 'Canon EOS 3000V zoom 28/80', 'https://via.placeholder.com/150x200', '329.00', 2),
('CAS07', 'Cassette DV60 par 5', 'https://via.placeholder.com/150x200', '26.90', 4),
('CP100', 'Caméscope Panasonic SV-AV 100', 'https://via.placeholder.com/150x200', '1490.00', 1),
('CS330', 'Caméscope Sony DCR-PC330', 'https://via.placeholder.com/150x200', '1629.00', 1),
('DEL30', 'Portable DELL X300', 'https://via.placeholder.com/150x200', '1715.00', 3),
('DVD75', 'DVD vierge par 3', 'https://via.placeholder.com/150x200', '17.50', 4),
('HP497', 'PC Bureau HP497 écran TFT', 'https://via.placeholder.com/150x200', '1500.00', 3),
('NIK55', 'Nikon F55+zoom 28/80', 'https://via.placeholder.com/150x200', '269.00', 2),
('NIK80', 'Nikon F80', 'https://via.placeholder.com/150x200', '479.00', 2),
('S0XMP', 'PC Portable Sony Z1-XMP', 'https://via.placeholder.com/150x200', '2399.00', 3),
('SAX15', 'Portable Samsung X15 XVM', 'https://via.placeholder.com/150x200', '1999.00', 3);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE `categorie` (
  `id_categorie` mediumint UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `nom`) VALUES
(1, 'vidéo'),
(2, 'photo'),
(3, 'informatique'),
(4, 'divers');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id_client` mediumint UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `ville` varchar(60) NOT NULL,
  `code_postal` varchar(5) NOT NULL,
  `id_users` mediumint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `nom`, `prenom`, `adresse`, `ville`, `code_postal`, `id_users`) VALUES
(1, 'Marti', 'Jean', '5 av Einstein', 'Orléans', '45100', 1),
(2, 'Rapp', 'Paul', '8 rue du Port', 'Paris', '75002', 2),
(3, 'Devos', 'Marie', '75 bd Hochimin', 'Lille', '59004', 3),
(4, 'Hochon', 'Paul', '33 rue Tsétsé', 'Chartres', '28000', 4),
(5, 'Grave', 'Nuyen', '75 bd Hochimin', 'Lille', '59002', 5),
(6, 'Hachette', 'Jeanne', '60 rue d’Amiens', 'Versaillles', '78010', 6),
(7, 'Marti', 'Pierre', '4 av. Henri', 'Paris', '75004', 7),
(8, 'Mac Neal', 'John', '89 rue Diana', 'Lyon', '69009', 8),
(9, 'Basile', 'Did', '26 rue Gallas', 'Nantes', '44100', 9),
(10, 'Darc', 'Jeanne', '9 av. d’Orléans', 'Paris', '75008', 10),
(11, 'Gaté', 'Bill', '9 bd des Bugs', 'Lyon', '69007', 11),
(12, 'Spencer', 'Marc', 'rue du blues', 'Orléans', '45100', 12),
(13, 'Spancer', 'Diss', 'Metad Street', 'Bordeaux', '33100', 13);

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE `commande` (
  `id_comm` mediumint UNSIGNED NOT NULL,
  `id_client` mediumint UNSIGNED NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id_comm`, `id_client`, `date`) VALUES
(1, 5, '2012-06-11'),
(2, 9, '2012-06-25'),
(3, 1, '2012-07-12'),
(4, 3, '2012-07-14'),
(5, 9, '2012-07-31'),
(6, 10, '2012-08-08'),
(7, 2, '2012-08-25'),
(8, 7, '2012-09-04'),
(9, 11, '2012-10-15'),
(10, 4, '2012-11-23'),
(11, 8, '2013-01-21'),
(12, 5, '2013-02-01'),
(13, 9, '2013-03-13'),
(14, 1, '2021-11-24');

-- --------------------------------------------------------

--
-- Structure de la table `ligne`
--

DROP TABLE IF EXISTS `ligne`;
CREATE TABLE `ligne` (
  `id_comm` mediumint UNSIGNED NOT NULL,
  `id_article` char(5) NOT NULL,
  `quantite` tinyint UNSIGNED NOT NULL,
  `prix_unit` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `ligne`
--

INSERT INTO `ligne` (`id_comm`, `id_article`, `quantite`, `prix_unit`) VALUES
(1, 'CAS07', 3, '26.90'),
(1, 'CS330', 1, '1629.00'),
(2, 'HP497', 2, '1500.00'),
(3, 'NIK80', 5, '479.00'),
(4, 'DVD75', 2, '17.50'),
(4, 'S0XMP', 3, '2399.00'),
(5, 'CA300', 1, '329.00'),
(6, 'CAS07', 3, '26.90'),
(6, 'CP100', 1, '1490.00'),
(7, 'SAX15', 5, '1999.00'),
(8, 'CP100', 1, '1490.00'),
(8, 'S0XMP', 1, '2399.00'),
(9, 'NIK55', 1, '269.00'),
(10, 'DEL30', 2, '1715.00'),
(10, 'SAX15', 1, '1999.00'),
(11, 'DVD75', 10, '17.50'),
(12, 'CAS07', 4, '26.90'),
(12, 'CS330', 3, '1629.00'),
(13, 'SAX15', 2, '1999.00'),
(14, 'CA300', 1, '329.00');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_users` mediumint UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_users`, `email`, `password`, `role`) VALUES
(1, 'mart@marti.com', '$argon2i$v=19$m=65536,t=4,p=1$OXBCeHU1R3ZTenVZb2NPVg$6wNLUKdYHfMPwAW5E4hEeJV4jB2r2vwwm/XRtnOhCSA', 'admin'),
(2, 'rapp@libert.com', '$argon2i$v=19$m=65536,t=4,p=1$VXAzb2h0Z1BVMkNiWkMzRw$7qn97Gf8VCi3a42sCKl+QksDxMeDbbzKZ8cKqxmu6cQ', 'user'),
(3, 'devos.marie@waladoo.fr', ' $argon2i$v=19$m=65536,t=4,p=1$MzNJck54VG5JRGxvZEhkLg$BMUCucY/1Kw45Z8dghTPcG13Oef0kb/Qq7IAW3VbYoI', 'user'),
(4, 'hoch@fiscali.fr', '$argon2i$v=19$m=65536,t=4,p=1$MzNJck54VG5JRGxvZEhkLg$BMUCucY/1Kw45Z8dghTPcG13Oef0kb/Qq7IAW3VbYoI', 'user'),
(5, 'grav@waladoo.fr', '$argon2i$v=19$m=65536,t=4,p=1$UVp5ZU95RkFqZ1IzNXlEeQ$9QRa6R2fiGn3GJy5PQVqyTtHiDW3a3yhN3ssMGxgDLM', 'user'),
(6, 'martin7@fiscali.fr', '$argon2i$v=19$m=65536,t=4,p=1$cERoM2RYZ3BoNzliNFd4Ug$m5GV7iDgyc0ZtiOVOf4nVKFQ0UZCx0XfZsuIAiKFScA', 'user'),
(7, 'pmartin@fiscali.fr', '$argon2i$v=19$m=65536,t=4,p=1$MWlIMlhRQ1VQcElTdG9oYg$By58k0rYQ31o/CdsjtxjpPtIcTinLTAqd714T5RFHFA', 'user'),
(8, 'mac@freez.fr', '$argon2i$v=19$m=65536,t=4,p=1$bDN2TEFXb0xNUlVVWjlEag$8qLsV+Dh9r8j88o0sCxTW5mJ/T8XhcVlsp+Ce9v1NXs', 'user'),
(9, 'bas@walabi.com', '$argon2i$v=19$m=65536,t=4,p=1$MEtIMUNkRjdrTGFyeXNraw$8t9U4FFxBQ1tDZz/aLT6RoIT1N2At0zn7HhdwE+7CU0', 'user'),
(10, 'darc.jeanne@waladoo.fr', '$argon2i$v=19$m=65536,t=4,p=1$bzVNbFZVR3RWa2NlR2dNWQ$7ljqS0M07R65IkjGRDs1DfmikIu0UoubGMKZhtCqzt4', 'user'),
(11, 'bill@midrohard.be', '$argon2i$v=19$m=65536,t=4,p=1$N095cFcxL2tqNFBhWGRlWA$OTSAAhAphPOeXB5eY9s1TIBzrrRX9r8dsKclDfsAzQ8', 'user'),
(12, 'marc.s@waladoo.fr', '$argon2i$v=19$m=65536,t=4,p=1$MzJ5RTVYMC9qLzlGeFFQUA$Oul7jCTjYhLTG77c/uRqVr24K3YTOsmnkPb3UJEGWmk', 'user'),
(13, 'diss@metad.fr', '$argon2i$v=19$m=65536,t=4,p=1$MTJORHA5NkxaTTEzVzVZcg$nHEXKwjI0D8DMh/nntC01Rge8reO37FMdLc8d0SuWQs', 'user');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id_article`),
  ADD KEY `article_ibfk_1` (`id_categorie`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `client_ibfk_1` (`id_users`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id_comm`,`id_client`),
  ADD KEY `id_client` (`id_client`);

--
-- Index pour la table `ligne`
--
ALTER TABLE `ligne`
  ADD PRIMARY KEY (`id_comm`,`id_article`),
  ADD KEY `id_article` (`id_article`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_users`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` mediumint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
