<?php
namespace App\Models;

class FormsModel
{
    protected $method;

    protected $enctype;

    protected $form;

    public function __construct(string $method = "GET", string $enctype = "x-www-form-urlencoded")
    {
        $this->method = strtoupper($method);
        $this->enctype = $enctype;
    }

    /**
     * Get the value of method
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Set the value of method
     *
     * @return  self
     */
    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get the value of enctype
     */
    public function getEnctype(): string
    {
        return $this->enctype;
    }

    /**
     * Set the value of enctype
     *
     * @return  self
     */
    public function setEnctype(string $enctype): self
    {
        $this->enctype = $enctype;

        return $this;
    }

    /**
     * create: creation du formulaire
     *
     * @return string
     */
    public function create(): string
    {
        return $this->form;
    }

    /**
     * debut:debut balise form
     *
     * @param  string $url
     * @param  array $attrib
     * @return self
     */
    public function addDebut(string $url = "#", array $attrib = []): self
    {
        $this->form = "<form method=\"{$this->getMethod()}\" action=\"$url\" enctype=\"{$this->getEnctype()}\"";
        $attrib ? $this->form .= $this->getAttributes($attrib) . ">" : $this->form .= ">";
        return $this;
    }

    /**
     * fin:fin balise form
     *
     * @return self
     */
    public function addFin(): self
    {
        $this->form .= "</form>";
        return $this;
    }

    /**
     * label:balise label
     *
     * @param  string $lbfor
     * @param  string $name
     * @param  array $attrib
     * @return self
     */
    public function addLabel(string $lbfor, string $name, array $attrib = []): self
    {
        $this->form .= "<label for=\"$lbfor\"";
        $attrib ? $this->form .= $this->getAttributes($attrib) . ">$name</label>" : $this->form .= ">$name</label>";
        return $this;
    }

    /**
     * input:balise input
     *
     * @param  string $type
     * @param  string $name
     * @param  array $attrib
     * @return self
     */
    public function addInput(string $type = 'text', string $name, array $attrib = []): self
    {
        $this->form .= "<input type=\"$type\" name=\"$name\"";
        $attrib ? $this->form .= $this->getAttributes($attrib) . ">" : $this->form .= ">";
        return $this;
    }

    /**
     * button:balise button
     *
     * @param  string $value
     * @param  string $type
     * @param  string $name
     * @param  array $attrib
     * @return self
     */
    public function addButton(string $value, string $name, array $attrib = [], string $type = "text"): self
    {
        $this->form .= "<button type=\"$type\" name=\"$name\" value=\"$value\"";
        $attrib ? $this->form .= $this->getAttributes($attrib) . ">$value</button>" : $this->form .= ">$value</button>";
        return $this;
    }
    /**
     * textarea:balise textarea
     *
     * @param  int $col
     * @param  int $row
     * @param  string $name
     * @param  array $attrib
     * @return self
     */
    public function addTextarea(string $name, array $attrib = []): self
    {
        $this->form .= "<textarea name=\"$name\"";
        $attrib ? $this->form .= $this->getAttributes($attrib) . "></textarea>" : $this->form .= "></textarea>";
        return $this;
    }

    /**
     * select:balise select
     *
     * @param  array $options
     * @param  string $name
     * @param  array $attrib
     * @return self
     */
    public function addSelect(string $name, array $options, array $attrib = []): self
    {
        $this->form .= "<select name=\"$name\"";
        $attrib ? $this->form .= $this->getAttributes($attrib) . ">" : $this->form .= ">";
        foreach ($options as $val => $texte) {
            $this->form .= "<option value=\"$texte\">$texte</option>";
        }
        $this->form .= "</select>";
        return $this;
    }

    /**
     * getAttributes()
     *
     * Vérifie si des éléments du tableau d'attributes sont dans la liste des booleans attributes et les concatènes
     * Sinon concatène sous la forme clé=valeur
     * Exemple1: required
     * Exemple2: name="nom"
     *
     * @param  array $attributes
     * @return string
     */
    public function getAttributes(array $attributes): string
    {
        $chaine = "";

        $boolAttributes = ['allowfullscreen', 'allowpaymentrequest', 'async', 'autofocus',
            'autoplay', 'checked', 'controls', 'default', 'defer', 'disabled', 'formnovalidate',
            'hidden', 'ismap', 'itemscope', 'loop', 'multiple', 'muted', 'nomodule', 'novalidate',
            'open', 'playsinline', 'readonly', 'required', 'reversed', 'selected', 'truespeed'];

        foreach ($attributes as $attribut => $valeur) {
            in_array(strtolower($attribut), $boolAttributes) ? $chaine .= " " . strtolower($attribut) : $chaine .= " " . strtolower($attribut) . "='$valeur'";
        };

        return $chaine;
    }

    public function addBalise(string $balise, array $attrib = []): self
    {
        $this->form .= "<$balise";
        $attrib ? $this->form .= $this->getAttributes($attrib) . ">" : $this->form .= ">";
        return $this;
    }

    public function addFinBalise(string $balise, array $attrib = []): self
    {
        $this->form .= "</$balise>";
        return $this;
    }
}