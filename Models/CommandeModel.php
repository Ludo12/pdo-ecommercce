<?php
namespace App\Models;

use PDO;

class CommandeModel extends Model
{
    protected $id_comm;

    protected $id_client;

    protected $date;

    public function __construct()
    {
        $this->table = "commande";
        $this->date = date("Y-m-d H:i:s");
    }

    /**
     * Get the value of id_comm
     */
    public function getId_comm(): int
    {
        return $this->id_comm;
    }

    /**
     * Set the value of id_comm
     *
     * @return  self
     */
    public function setId_comm(int $id_comm): self
    {
        $this->id_comm = $id_comm;

        return $this;
    }

    /**
     * Get the value of id_client
     */
    public function getId_client(): int
    {
        return $this->id_client;
    }

    /**
     * Set the value of id_client
     *
     * @return  self
     */
    public function setId_client(int $id_client): self
    {
        $this->id_client = $id_client;

        return $this;
    }

    /**
     * Get the value of date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * getArticlesCommande
     *
     * @param  int $idClient
     * @return array
     */
    public function getArticlesCommande(int $idClient): array
    {
        return $this->requete("SELECT c.id_comm,c.id_client,c.date, l.quantite, a.id_article,a.designation,a.image,a.prix
        FROM commande AS c
           INNER JOIN
           ligne AS l
           ON c.id_comm = l.id_comm
           INNER JOIN
           article AS a
           ON l.id_article = a.id_article
           WHERE c.id_client=$idClient")->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * getArticlesCommande
     *
     * @param  int $idClient
     * @return array
     */
    public function getAdminCommande(): array
    {
        return $this->requete("SELECT c.id_comm,cl.nom,cl.prenom,c.date, l.quantite, a.id_article,a.designation,a.prix
        FROM commande AS c
           INNER JOIN
           ligne AS l
           ON c.id_comm = l.id_comm
           INNER JOIN
           article AS a
           ON l.id_article = a.id_article
           INNER JOIN
           client AS cl
           ON c.id_client = cl.id_client
           ORDER BY c.date DESC,cl.nom ASC")->fetchAll(PDO::FETCH_ASSOC);
    }
}