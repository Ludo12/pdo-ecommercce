<?php
namespace App\Models;

class UsersModel extends Model
{
    protected $id_users;

    protected $email;

    protected $password;

    protected $role;

    public function __construct()
    {
        $this->table = "users";
    }

    /**
     * Get the value of id_users
     */
    public function getId_users()
    {
        return $this->id_users;
    }

    /**
     * Set the value of id_users
     *
     * @return  self
     */
    public function setId_users($id_users)
    {
        $this->id_users = $id_users;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_ARGON2I);

        return $this;
    }

    /**
     * Get the value of role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set the value of role
     *
     * @return  self
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * findByEmail
     *
     * @param  string $email
     * @return bool|self
     */
    public function findByEmail(string $email): bool | self
    {
        return $this->requete("SELECT * FROM {$this->table} WHERE email=?", [$email])->fetch();
    }

    /**
     * generateChecksum:aldo de Luhn
     *
     * @param  int $value
     * @return int
     */
    public function generateChecksum(int $value): int
    {
        if (!is_numeric($value)) {
            throw new \InvalidArgumentException(__FUNCTION__ . ' can only accept numeric values.');
        }

        // Forcer la valeur à être une chaîne de string.
        $value = (string) $value;

        // Définir des valeurs initiales.
        $length = strlen($value);
        $parity = $length % 2;
        $sum = 0;

        for ($i = $length - 1; $i >= 0; --$i) {
            // Extraire un caractère de la valeur.
            $char = $value[$i];
            if ($i % 2 != $parity) {
                $char *= 2;
                if ($char > 9) {
                    $char -= 9;
                }
            }
            // Ajoute le caractère à la somme des caractères.
            $sum += $char;
        }

        // Renvoie la valeur de la somme multipliée par 9 puis modulo 10.
        return ($sum * 9) % 10;
    }
}