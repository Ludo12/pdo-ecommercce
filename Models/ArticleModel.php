<?php
namespace App\Models;

class ArticleModel extends Model
{
    protected $id_article;

    protected $designation;

    protected $prix;

    protected $id_categorie;

    protected $image;

    public function __construct()
    {
        $this->table = "article";
    }

    /**
     * Get the value of id_article
     */
    public function getId_article(): string
    {
        return $this->id_article;
    }

    /**
     * Set the value of id_article
     *
     * @return  self
     */
    public function setId_article(string $id_article): self
    {
        $this->id_article = $id_article;

        return $this;
    }

    /**
     * Get the value of designation
     */
    public function getDesignation(): string
    {
        return $this->designation;
    }

    /**
     * Set the value of designation
     *
     * @return  self
     */
    public function setDesignation(string $designation): self
    {
        $this->designation = $designation;

        return $this;
    }

    /**
     * Get the value of prix
     */
    public function getPrix(): float
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */
    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of id_categorie
     */
    public function getId_categorie(): int
    {
        return $this->id_categorie;
    }

    /**
     * Set the value of id_categorie
     *
     * @return  self
     */
    public function setId_categorie(int $id_categorie): self
    {
        $this->id_categorie = $id_categorie;

        return $this;
    }

    /**
     * getCategorie: Donne le nom de la categorie
     *
     * @return string
     */
    public function getCategorie(): string
    {
        $id = $this->getId_categorie();
        return $this->requete("SELECT c.nom FROM categorie c,article a WHERE c.id_categorie=$id")->fetch()->nom;
    }

    /**
     * getRecherche
     *
     * @param  array $arrSearch Tableau des éléments recherchés
     * @return array
     */
    public function getRecherche(array $arrSearch): array
    {
        $params = [];
        $like = [];

        //si désignation n'est pas vide
        if (!empty($arrSearch['designation'])) {
            $params['designation'] = $arrSearch['designation'];
            $like[] = 'designation';
        }

        //si catégorie n'est pas égale à toutes les catégories
        if ($arrSearch['categorie'] !== 'Toutes les catégories') {
            $params['id_categorie'] = (int) $arrSearch['categorie']->getId_categorie();
        }

        //si params vide on retourne tous les articles sinon le resultat de la recherche
        return (empty($params)) ? $this->findAll("ORDER BY $this->table.designation ASC") : $this->findBy($params, $like, ['designation', 'ASC']);
    }

    /**
     * Get the value of image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of image
     *
     * @return  self
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }
}