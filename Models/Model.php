<?php
namespace App\Models;

use App\Core\Db;
use PDO;
use PDOStatement;

class Model extends Db
{
    // Table de la base de données
    protected $table;

    // Instance de connexion
    private $db;

    /**
     * requete
     *
     * @param  string $sql requete sql
     * @param  array $params tableau de paramètres
     * @return PDOStatement
     */
    public function requete(string $sql, array $params = null): PDOStatement
    {
        // On récupère l'instance de Db
        $this->db = Db::run();

        //on vérifie si params[] est null
        if ($params !== null) {
            //requete préparée
            //$sql="INSERT INTO personnes (prenom,nom) VALUES (:unPrenom,:unNom)";
            //$modele = $this->db-> prepare($sql);
            //$modele -> execute(array(':unPrenom' => 'Cémoi',':unNom' => 'Toto'));
            $modele = $this->db->prepare($sql);
            $modele->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
            $modele->execute($params);
            return $modele;
        } else {
            //requéte simple
            //$sql = "SELECT * FROM personnes";
            //$modele = $this->db->query($sql);
            $modele = $this->db->query($sql);
            $modele->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
            return $modele;
        }
    }

    /**
     * Sélection de tous les enregistrements d'une table
     * @param string $orderBy ordre de tri exemple "order by table.champ ASC"
     * @return array Tableau des enregistrements trouvés
     */
    public function findAll(string $orderBy = null): array
    {
        $res = $this->requete('SELECT * FROM ' . $this->table . " " . $orderBy);
        return $res->fetchAll();
    }

    /**
     * Sélection d'enregistrements d'une table en fonction de critère de recherche
     * @param array $params critère de recherche, exemple : [champ_table=>valeur]
     * @param array $arrayLike liste des champs à rechercher avec LIKE, exemple : [champ_table1,champ_table2]
     * @param array $arrayOrder ordre de recherche, exemple : ['mon_champ',direction]
     * @return array|self array ou un objet d'enregistrements trouvés selon si fetch ou fetchAll
     */
    public function findBy(array $params, array $arrayLike = null, array $arrayOrder = null, bool $fetchAll = true): array | self
    {
        // On boucle pour "éclater le tableau"
        $champs = [];
        $values = [];
        foreach ($params as $champ => $value) {
            if (empty($arrayLike) || is_null($arrayLike) || !in_array($champ, $arrayLike)) {
                $champs[] = "$champ = ?";
                $values[] = $value;
            } else {
                $champs[] = "$champ LIKE ?";
                $values[] = "%" . $value . "%";
            }
        }

        $order = "";
        if (!empty($arrayOrder) || !is_null($arrayOrder)) {
            $order = " ORDER BY $arrayOrder[0] " . strtoupper($arrayOrder[1]);
        };

        // On transforme le tableau en chaîne de caractères séparée par des AND
        $liste_champs = implode(' AND ', $champs);

        // On exécute la requête
        $req = $this->requete("SELECT * FROM {$this->table} WHERE $liste_champs $order", $values);
        return ($fetchAll) ? $req->fetchAll() : $req->fetch();
    }

    /**
     * Sélection d'un enregistrement suivant son id
     * @param mixed $id id de l'enregistrement
     * @return array Tableau contenant l'enregistrement trouvé
     */
    public function find($id): self
    {

        $champ_id = "id" . "_" . $this->table;

        // On exécute la requête
        return $this->requete("SELECT * FROM {$this->table} WHERE $champ_id = ?", [$id])->fetch();
    }

    /**
     * Insertion d'un enregistrement suivant un tableau de données
     * @param Model $model Objet à créer
     * @return bool
     */
    public function create()
    {
        $champs = [];
        $inter = [];
        $valeurs = [];

        // On boucle pour éclater le tableau
        foreach ($this as $champ => $valeur) {
            // INSERT INTO citation (idauteur, texte) VALUES (?, ?)
            if ($valeur !== null && $champ != 'db' && $champ != 'table') {
                $champs[] = $champ;
                $inter[] = "?";
                $valeurs[] = $valeur;
            }
        }

        // On transforme le tableau "champs" en une chaine de caractères
        $liste_champs = implode(', ', $champs);
        $liste_inter = implode(', ', $inter);

        // On exécute la requête
        return $this->requete('INSERT INTO ' . $this->table . ' (' . $liste_champs . ')VALUES(' . $liste_inter . ')', $valeurs);
    }

    /**
     * Mise à jour d'un enregistrement suivant un tableau de données
     * @param int $id id de l'enregistrement à modifier
     * @param Model $model Objet à modifier
     * @return bool
     */
    public function update(int $id)
    {
        $champs = [];
        $valeurs = [];
        // On boucle pour éclater le tableau
        foreach ($this as $champ => $valeur) {
            // UPDATE citation SET idauteur = ?, texte = ? WHERE idcit= ?
            if ($valeur !== null && $champ != 'db' && $champ != 'table') {
                $champs[] = "$champ = ?";
                $valeurs[] = $valeur;
            }
        }
        $champ_id = $champs[0];
        $valeurs[] = $id;

        // On transforme le tableau "champs" en une chaine de caractères
        $liste_champs = implode(', ', $champs);

        // On exécute la requête
        return $this->requete('UPDATE ' . $this->table . ' SET ' . $liste_champs . ' WHERE ' . $champ_id, $valeurs);
    }

    /**
     * Suppression d'un enregistrement
     * @param int $id id de l'enregistrement à supprimer
     * @return bool
     */
    public function delete(int $id)
    {
        $champ_id = "id" . "_" . $this->table;

        return $this->requete("DELETE FROM {$this->table} WHERE $champ_id = ?", [$id]);
    }

    /**
     * lastId
     *
     * @return void
     */
    public function lastId()
    {
        return $this->db->lastInsertId();
    }

    /**
     * Hydratation des données
     * @param array $donnees Tableau associatif des données
     * @return self Retourne l'objet hydraté
     */
    public function hydrate($donnees): self
    {
        foreach ($donnees as $key => $value) {
            // On récupère le nom du setter correspondant à l'attribut.
            $method = 'set' . ucfirst($key);

            // Si le setter correspondant existe.
            if (method_exists($this, $method)) {
                // On appelle le setter.
                $this->$method($value);
            }
        }
        // var_dump($this);
        return $this;
    }

    /**
     * serialisation des données
     *
     * @return array Retourne l'array sérialisé
     */
    public function serialisation(): array
    {
        $arrayDonnee = [];
        foreach ($this as $key => $value) {
            // On récupère le nom du getter correspondant à l'attribut.
            $method = 'get' . ucfirst($key);

            // Si le getter correspondant existe.
            if (method_exists($this, $method)) {
                // On appelle le setter.
                $arrayDonnee[$key] = $this->$method();
            }
        };
        return $arrayDonnee;
    }

}