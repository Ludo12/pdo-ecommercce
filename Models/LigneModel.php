<?php
namespace App\Models;

class LigneModel extends Model
{
    protected $id_comm;

    protected $id_article;

    protected $quantite;

    protected $prix_unit;

    public function __construct()
    {
        $this->table = "ligne";
    }

    /**
     * Get the value of id_comm
     */
    public function getId_comm(): int
    {
        return $this->id_comm;
    }

    /**
     * Set the value of id_comm
     *
     * @return  self
     */
    public function setId_comm(int $id_comm): self
    {
        $this->id_comm = $id_comm;

        return $this;
    }

    /**
     * Get the value of id_article
     */
    public function getId_article(): string
    {
        return $this->id_article;
    }

    /**
     * Set the value of id_article
     *
     * @return  self
     */
    public function setId_article(string $id_article): self
    {
        $this->id_article = $id_article;

        return $this;
    }

    /**
     * Get the value of quantite
     */
    public function getQuantite(): int
    {
        return $this->quantite;
    }

    /**
     * Set the value of quantite
     *
     * @return  self
     */
    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get the value of prix_unit
     */
    public function getPrix_unit(): float
    {
        return $this->prix_unit;
    }

    /**
     * Set the value of prix_unit
     *
     * @return  self
     */
    public function setPrix_unit(float $prix_unit): self
    {
        $this->prix_unit = $prix_unit;

        return $this;
    }

    public static function plus(int $value)
    {
        if (isset($_SESSION['commande'])) {
            $value++;
        }
    }

    public static function moins(int $value)
    {
        if (isset($_SESSION['commande']) && $value > 0) {
            $value--;
        }
    }
}