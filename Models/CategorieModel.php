<?php
namespace App\Models;

class CategorieModel extends Model
{
    protected $id_categorie;

    protected $nom;

    public function __construct()
    {
        $this->table = "categorie";
    }

    /**
     * Get the value of id_categorie
     */
    public function getId_categorie(): int
    {
        return $this->id_categorie;
    }

    /**
     * Set the value of id_categorie
     *
     * @return  self
     */
    public function setId_categorie(int $id_categorie): self
    {
        $this->id_categorie = $id_categorie;

        return $this;
    }

    /**
     * Get the value of nom
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
