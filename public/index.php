<?php
define('ROOT', dirname(__DIR__));
define('VIEWS', ROOT . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']));

use App\Autoloader;
use App\Core\Router;

// On charge le fichier Autoloader
require_once ROOT . '/autoloader.php';
// On charge la méthode statique
Autoloader::register();
$router = new Router($_GET['url']);

$router->get('/articles', 'ArticleController@index');
$router->get('/articles/:id', 'ArticleController@show');
$router->post('/stock', 'ArticleController@stock');

$router->get('/login', 'UsersController@login');
$router->post('/login', 'UsersController@loginPost');
$router->get('/logout', 'UsersController@logout');

$router->get('/signup', 'UsersController@signup');
$router->post('/signup', 'UsersController@signupPost');
$router->get('/mon-espace', 'UsersController@monEspace');

$router->get('/panier', 'CommandeController@showCommande');
$router->post('/panier', 'CommandeController@validerCommande');

$router->get('/admin', 'AdminController@allCommande');
$router->run();