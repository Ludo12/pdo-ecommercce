<?php
namespace App\Controllers;

use App\Models\ArticleModel;
use App\Models\ClientModel;
use App\Models\CommandeModel;
use App\Models\LigneModel;
use App\Models\UsersModel;
use App\Validation\Validator;

class CommandeController extends Controller
{
    public function validerCommande()
    {
        if (!$this->isConnected()) {
            header('Location: /login', true, 302);
        }
        $msg = "";

        //On Vérifie le formulaire
        $validator = new Validator($_POST);
        //On stotck les erreurs de "quantity"
        $errors = $validator->validate([
            'paiement' => ['required'],
        ]);

        if (!empty($errors)) {
            $paiement = strip_tags(trim($_POST['paiement']));
            header('Location: /panier?erreur=paiement');
        }

        if (isset($_POST['achat']) && isset($_POST['paiement']) &&
            !empty($_POST['paiement']) && $_POST['achat'] === 'achat') {
            $paiement = (int) strip_tags(trim($_POST['paiement']));

            $usersModel = new UsersModel();
            $user = $usersModel->find((int) $_SESSION['user']);

            // Extraire la somme de contrôle.
            $extractedChecksum = substr($paiement, -1);

            // Extraire la valeur.
            $extractedValue = substr($paiement, 0, -1);

            // Calcule la somme de contrôle sur la valeur extraite.
            $calculatedChecksum = $user->generateChecksum($extractedValue);

            // Comparez !
            if ((int) $extractedChecksum === $calculatedChecksum) {

                $client = (new ClientModel)->findBy(params:['id_users' => $user->getId_users()], fetchAll:false);

                $commandeModel = new CommandeModel();

                $commande = $commandeModel->setId_client($client->getId_client());
                $commande->create();
                $id_comm = (int) $commande->lastId();
                foreach ($_SESSION['commande']['ligne'] as $commande) {
                    $id = $commande['ref'];
                    $qte = $commande['qte'];
                    $article = (new ArticleModel)->find($id);

                    $ligne = (new LigneModel)->setId_comm($id_comm)
                        ->setId_article($article->getId_article())
                        ->setQuantite($qte)
                        ->setPrix_unit($article->getPrix());

                    $ligne->create();
                };
                $msg = "La commande a bien été enregistrée";
                unset($_SESSION['commande']);
                header('Location: /mon-espace');
            }
        }
        return $this->render('commande.panier', compact('msg'));
    }

    public function showCommande()
    {
        if (!$this->isConnected()) {
            header('Location: /login', true, 302);
        }

        $articles = [];
        $total = 0;
        if (isset($_SESSION['commande'])) {
            foreach ($_SESSION['commande']['ligne'] as $panier) {
                $article = (new ArticleModel)->findBy(['id_article' => $panier['ref']])[0]->serialisation();
                $article['quantite'] = $panier['qte'];
                $total += ($article['quantite'] * $article['prix']);
                $articles[] = $article;
            };
        };

        return $this->render('commande.panier', compact('articles', 'total'));
    }
}