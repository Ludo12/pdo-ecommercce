<?php
namespace App\Controllers;

class Controller
{
    public function __construct()
    {
        if (session_status() === PHP_SESSION_NONE) {session_start();}
    }

    /**
     * render de vue
     *
     * @param  string $path
     * @param  array $params
     * @return void
     */
    public function render(string $path, array $params = null)
    {
        //On démarre le buffer
        ob_start();

        //On remplace les '.' par un separateur
        $path = str_replace('.', DIRECTORY_SEPARATOR, $path);

        //On vérifie s'il y a des paramètre
        if ($params) {
            extract($params);
        }
        //On integre la vue du $path
        require VIEWS . $path . '.php';

        //On stock la vue
        $content = ob_get_clean();

        //On integre la vue du layout
        require VIEWS . 'layout.php';
    }

    /**
     * isConnected: vérifie si le user est connecté
     *
     * @return bool
     */
    public function isConnected(): bool
    {
        return (isset($_SESSION['isConnected']) && $_SESSION['isConnected'] === true) ? true : false;
    }

    /**
     * isAdmin: vérifie si le user est admin
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return (isset($_SESSION['auth']) && $_SESSION['auth'] === 'admin') ? true : false;
    }
}