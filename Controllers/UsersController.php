<?php
namespace App\Controllers;

use App\Models\ClientModel;
use App\Models\CommandeModel;
use App\Models\UsersModel;
use App\Validation\Validator;

class UsersController extends Controller
{
    /**
     * login
     *
     * @return void
     */
    public function login()
    {

        return $this->render('auth.login');
    }

    /**
     * logout
     *
     * @return void
     */
    public function logout()
    {
        session_destroy();
        unset($_SESSION);

        return $this->render('auth.login');
    }

    /**
     * loginPost
     *
     * @return void
     */
    public function loginPost()
    {
        //On Vérifie le formulaire
        $validator = new Validator($_POST);
        //On stotck les erreurs de "email"
        $errors = $validator->validate([
            'email' => ['email', 'required'],
            'password' => ['min=6', 'required'],
        ]);

        if (isset($_POST['email']) && !empty($_POST['email']) && isset($_POST['password']) && !empty($_POST['password'])) {
            $email = strip_tags(trim($_POST['email']));
            $password = strip_tags(trim($_POST['password']));

            $user = (new UsersModel)->findByEmail($email);

            if (!$user) {
                $errors['connexion'] = "Le mail et/ou le mot de passe sont incorrectes";
            };

            if (empty($errors)) {
                if (password_verify($password, $user->getPassword())) {
                    $_SESSION['auth'] = $user->getRole();
                    $_SESSION['isConnected'] = true;
                    $_SESSION['user'] = $user->getId_users();
                    (isset($_SESSION['commande'])) ? header('Location: /panier/encours') : header('Location: articles');
                } else {
                    $errors['connexion'] = "Le mail et/ou le mot de passe sont incorrectes";
                };
            }
        }
        return $this->render('auth.login', compact('email', 'errors'));
    }

    public function signup()
    {

        return $this->render('auth.signup');
    }

    public function signupPost()
    {
        //On Vérifie le formulaire
        $validator = new Validator($_POST);
        //On stotck les erreurs de "email"
        $errors = $validator->validate([
            'nom' => ['min=2', 'required'],
            'prenom' => ['min=20', 'required'],
            'adresse' => ['min=2', 'required'],
            'ville' => ['min=2', 'required'],
            'code_postal' => ['min=5', 'max=5', 'required'],
            'email' => ['email', 'required'],
            'password' => ['min=6', 'required'],
            'password2' => ['min=6', 'password2', 'required'],
        ]);

        if (isset($_POST['email']) && !empty($_POST['email']) &&
            isset($_POST['password']) && !empty($_POST['password']) &&
            isset($_POST['password2']) && !empty($_POST['password2']) &&
            isset($_POST['nom']) && !empty($_POST['nom']) &&
            isset($_POST['prenom']) && !empty($_POST['prenom']) &&
            isset($_POST['adresse']) && !empty($_POST['adresse']) &&
            isset($_POST['ville']) && !empty($_POST['ville']) &&
            isset($_POST['code_postal']) && !empty($_POST['code_postal'])
        ) {
            $email = strip_tags(trim($_POST['email']));
            $password = strip_tags(trim($_POST['password']));
            $nom = strip_tags(trim($_POST['nom']));
            $prenom = strip_tags(trim($_POST['prenom']));
            $adresse = strip_tags(trim($_POST['adresse']));
            $ville = strip_tags(trim($_POST['ville']));
            $code_postal = strip_tags(trim($_POST['code_postal']));

            if (empty($errors)) {
                $userModel = new UsersModel();
                $emailFind = $userModel->findByEmail($email);

                if ($emailFind) {
                    $errors['inscription'] = "Erreur lors de l'inscription";
                    header('Location: /signup', true, 302);
                    exit();
                };

                $user = $userModel->setEmail($email)
                    ->setPassword($password)
                    ->setRole('user');

                $user->create();
                $idUser = $user->lastId();

                $clientModel = new ClientModel();
                $client = $clientModel->setNom($nom)
                    ->setPrenom($prenom)
                    ->setAdresse($adresse)
                    ->setVille($ville)
                    ->setCode_postal($code_postal)
                    ->setId_users($idUser);
                $client->create();
                header('Location: /login', true, 301);
                exit();
            } else {
                return $this->render('auth.signup', compact('errors', 'nom', 'prenom', 'adresse', 'ville', 'code_postal', 'email'));
            };
        };
    }

    public function monEspace()
    {
        if (!$this->isConnected()) {
            header('Location: /login', true, 302);
        };

        $commandeModel = new CommandeModel();
        $client = (new ClientModel)->findBy(params:['id_users' => (int) $_SESSION['user']], fetchAll:false);

        $commandes = $commandeModel->getArticlesCommande($client->getId_client());

        $msg = "Bienvenu " . $client->getPrenom() . " " . $client->getNom();
        return $this->render('user.index', compact('msg', 'client', 'commandes'));
    }
}