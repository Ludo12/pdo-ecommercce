<?php
namespace App\Controllers;

use App\Models\CommandeModel;

class AdminController extends Controller
{
    public function allCommande()
    {
        if (!$this->isConnected() || !$this->isAdmin()) {
            header('Location: /login', true, 302);
        };

        $commandeModel = new CommandeModel();

        $commandes = $commandeModel->getAdminCommande();
        return $this->render('admin.index', compact('commandes'));
    }
}