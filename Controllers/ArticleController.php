<?php
namespace App\Controllers;

use App\Models\ArticleModel;
use App\Models\CategorieModel;
use App\Models\FormsModel;
use App\Validation\Validator;

class ArticleController extends Controller
{
    public function index()
    {
        //On réinitialise session['search']
        if ($_SERVER['QUERY_STRING'] === "url=articles") {
            unset($_SESSION['search']);
        };

        //Instentiation de ArticleModel et CategorieModel
        $articleModel = new ArticleModel();
        $categorieModel = new CategorieModel();

        //Tous les articles
        $articles = $articleModel->findAll();
        //Toutes les categories
        $categories = $categorieModel->findAll();
        //On veut que le nom des categories
        $nomCategorie = ['Toutes les catégories'];
        foreach ($categories as $categorie) {
            $nomCategorie[] = $categorie->getNom();
        };

        //Instatiation du formulaire
        $form = new FormsModel();

        //On génère les balises souhaitées pour le formulaire
        $form->addDebut(attrib:["class" => "row g-3 align-items-center", "name" => "form"])
            ->addBalise(balise:"div", attrib:["class" => "col-auto"])
            ->addInput(type:'text', name:'search', attrib:['class' => 'form-control', 'placeholder' => "Recherche"])
            ->addFinBalise(balise:"div")
            ->addBalise(balise:"div", attrib:["class" => "col-auto"])
            ->addSelect(name:'categorie', options:$nomCategorie, attrib:["class" => "form-select"])
            ->addFinBalise(balise:"div")
            ->addBalise(balise:"div", attrib:["class" => "col-auto"])
            ->addButton(value:'Rechercher', name:'rechercher', attrib:['class' => 'btn btn-outline-primary'])
            ->addFinBalise(balise:"div")
            ->addFin();

        $msg = "";

        //Si search et categorie existe et que le formulaire soit soumis
        if (isset($_GET['rechercher']) && isset($_GET['search']) && isset($_GET['categorie'])) {
            //On nettoye
            $designation = strip_tags(trim($_GET['search']));
            $categorie = strip_tags(trim($_GET['categorie']));

            //On check categorie
            if (!empty($categorie) && $categorie !== 'Toutes les catégories') {
                $id_categorie = (new CategorieModel())->requete("SELECT id_categorie FROM categorie WHERE nom='{$categorie}'")->fetch();
            } elseif ($categorie === 'Toutes les catégories') {
                $id_categorie = $categorie;
            };

            //si id_categorie est vrai
            if ($id_categorie) {
                $arraySearch = ['designation' => $designation, 'categorie' => $id_categorie];
                $articles = $articleModel->getRecherche($arraySearch);

                //s'il y a déjà eu une recherche avant, on réinitialise $_SESSION['search']
                if (isset($_SESSION['search'])) {
                    unset($_SESSION['search']);
                }

                //on serialise
                foreach ($articles as $key => $article) {
                    $_SESSION['search'][$key] = $article->serialisation();
                    $_SESSION['search'][$key]['categorie'] = $article->getCategorie();
                };
            };

            //si search est vide on génère un msg d'erreur
            if (empty($articles)) {
                if (!$articles) {
                    $msg = "Aucun articles trouvés";};
                if (!$id_categorie) {
                    $msg = "La catégorie n'existe pas";
                };
            };
        };

        //On Vérifie le formulaire
        $validator = new Validator($_GET);
        //Onstotck les erreurs de "order"
        $errors = $validator->validate([
            'order' => ['checked' => ['prix', 'designation']],
        ]);

        //boolean pour check le prix ou la designation
        $checkPrix = false;
        $checkDesign = true;
        //Si pas d'erreure et que order existe et non vide
        if (!$errors && isset($_GET['order']) && !empty($_GET['order'])) {
            $order = strip_tags(trim($_GET['order']));
            //On verifie qui a été checked
            switch ($order) {
                case 'prix':
                    $checkPrix = true;
                    $checkDesign = false;
                    break;
                case 'designation':
                    $checkDesign = true;
                    $checkPrix = false;
                    break;
                default:
                    # code...
                    break;
            }

            //on tri par ordre ASC selon si "prix" ou "designation"
            usort($_SESSION['search'], function ($item1, $item2) {
                $order = strip_tags(trim($_GET['order']));
                return $item1[$order] <=> $item2[$order];
            });

            $articles = [];
            //on transforme le tableau en objet articleModel
            foreach ($_SESSION['search'] as $obj) {
                $articles[] = (new ArticleModel)->hydrate($obj);
            };
        };

        //Controle d'erreur sur la quantité d'achat. Vérification des limites du la fonction stock
        if (isset($_GET['errors'])) {
            $errors['quantity'][] = "Erreur sur la quantité de l'article demandé. La commande a échouée";}

        //On rend la vue
        return $this->render('article.index', [
            'articles' => $articles,
            'form' => $form->create(),
            'checkPrix' => $checkPrix,
            'checkDesign' => $checkDesign,
            'errors' => $errors,
            'msg' => $msg,
        ]);
    }

    public function show(string $id)
    {
        return $this->render('article.show', compact('id'));
    }

    public function stock()
    {
        if (isset($_POST['panier']) && strip_tags(trim($_POST['panier'])) === "panier") {
            http_response_code(301);
            header('Location: /panier');
            exit();
        };

        //On Vérifie le formulaire
        $validator = new Validator($_POST);
        //On stotck les erreurs de "quantity"
        $errors = $validator->validate([
            'quantity' => ['limite' => ['min=1', 'max=5']],
        ]);
        if (!empty($errors)) {
            http_response_code(301);
            header('Location: articles?errors=quantity');
            exit();
        };
        //On stock le panier
        if (empty($errors) && isset($_POST['article']) && !empty($_POST['article'])
            && isset($_POST['quantity']) && !empty($_POST['quantity'])) {

            //On nettoye et stock
            $article = explode("Réf : ", strip_tags(trim($_POST['article'])));
            $ref = $article[1];
            $qte = (int) strip_tags(trim($_POST['quantity']));
            if (isset($_SESSION['commande'])) {
                $arrayRef = array_column($_SESSION['commande']['ligne'], 'ref');
                if (in_array($ref, $arrayRef)) {
                    $key = array_keys($arrayRef, $ref)[0];
                    $_SESSION['commande']['ligne'][$key]['qte'] += 1;
                } else {
                    $_SESSION['commande']['ligne'][] = [
                        'ref' => $ref,
                        'qte' => $qte];
                };
            } else {
                $_SESSION['commande']['ligne'][] = [
                    'ref' => $ref,
                    'qte' => $qte];
            };

            if (isset($_POST['ajouter']) && strip_tags(trim($_POST['ajouter'])) === "ajouter") {
                http_response_code(301);
                header('Location: articles');
                exit();
            };
        };
    }
}