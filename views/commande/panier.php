<h1>Commande en cours</h1>
<?php
if (isset($msg)) {printf("%s %s %s", "<p>", $msg, "</p>");}
if (isset($_SESSION['commande'])):
?>
<form action="/panier" method="post">
    <?php
foreach ($articles as $article):
?>

    <div class="card my-3" style="max-width: 540px;">
        <div class="row g-0">
            <div class="col-md-4 my-auto text-center">
                <img src="<?=$article['image'];?>" class="img-fluid rounded-start" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <div class="d-flex flex-row align-items-baseline">
                        <div class="card-title fs-4 col-sm-5">Référence :</div>
                        <input type="text" class="form-control-plaintext fs-4 col-sm-2" name="ref"
                            value="<?=$article['id_article'];?>" disabled>
                    </div>
                    <input type="text" name="" class="card-text form-control-plaintext"
                        value="<?=$article['designation'];?>" disabled>
                    <div class="d-flex flex-row align-items-baseline">
                        <label class="card-title col-sm-5">Prix Unitaire :</label>
                        <input type="text" class="form-control-plaintext col-sm-2" name="ref"
                            value="<?=$article['prix'];?>€" disabled>
                    </div>
                    <div class="d-flex flex-row align-items-baseline">
                        <label class="card-title col-sm-5">Quantité :</label>
                        <input type="text" class="form-control-plaintext col-sm-2" name="qte"
                            value="<?=$article['quantite'];?>" disabled>
                    </div>
                    <p class="card-text mt-1">Montant :
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$article['prix'] * $article['quantite'];?>
                        €
                    </p>
                </div>
            </div>
        </div>
    </div>
    <?php
endforeach;
?>
    <div>
        <h3>Montant Total</h3>
        <p><?=$total;?>€</p>
    </div>
    <div class="mb-3">
        <label for="paiement" class="form-label">Carte bleu</label>
        <input type="paiement"
            class="form-control<?=(isset($_GET['erreur']) && $_GET['erreur'] === 'paiement') ? " is-invalid" : ""?>"
            style="width: 200px;" name="paiement" id="paiement" required>
        <?php if (isset($_GET['erreur']) && $_GET['erreur'] === 'paiement') {
    printf('%s %s %s', "<div class=\"invalid-feedback\">", "Le champ est obligatoire", "</div>");

}
?>
    </div>
    <button type="submit" name="achat" value="achat">Achat</button>
</form>
<?php
else:printf('%s', "<div>Le panier est vide</div>");
endif;
?>