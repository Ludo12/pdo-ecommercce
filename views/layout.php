<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'style.css'?>">
    <link rel="stylesheet" href="<?=SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'pme.css'?>">
    <title>$titre</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand text-light" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active text-light" aria-current="page" href="/articles">Articles</a>
                    </li>
                </ul>
                <ul class="navbar-nav d-lg-flex justify-content-lg-end">
                    <li class="nav-item">
                        <a href="/panier"
                            class="btn btn-outline-primary mb-2 mb-lg-0 <?=(isset($_SESSION['commande'])) ? "mx-lg-3" : "mx-lg-2"?> position-relative"
                            value="panier">
                            Panier
                            <?php if (isset($_SESSION['commande'])): ?>
                            <span
                                class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger">
                                <?=count(array_column($_SESSION['commande']['ligne'], 'ref'));?>
                            </span>
                            <?php endif;?>
                        </a>
                    </li>
                    <?php if (!isset($_SESSION['auth'])): ?>
                    <li class="nav-item">
                        <a href="/login" class="btn btn-outline-success mb-2 mb-lg-0 mx-lg-2" value="login">
                            Connexion
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="/signup" class="btn btn-outline-success mb-2 mb-lg-0 mx-lg-2" value="signup">
                            Inscription
                        </a>
                    </li>
                    <?php else: ?>
                    <?php if ($_SESSION['auth'] === 'admin'): ?>
                    <li class="nav-item">
                        <a href="/admin" class="btn btn-outline-warning mb-2 mb-lg-0 mx-lg-2" value="espace">
                            Admin
                        </a>
                    </li>
                    <?php endif;?>
                    <li class="nav-item">
                        <a href="/mon-espace" class="btn btn-outline-secondary mb-2 mb-lg-0 mx-lg-2" value="espace">
                            Mon espace
                        </a>
                    </li>
                    <li class="nav-item end-0">
                        <a href="/logout" class="btn btn-outline-danger mb-2 mb-lg-0 mx-lg-2" value="logout">
                            Deconnexion
                        </a>
                    </li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <?=$content?>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
</body>

</html>