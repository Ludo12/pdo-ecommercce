<h1 class="m-3">Panneau d'administration</h1>
<table class="table table-dark table-hover">
    <thead>
        <tr>
            <th scope="col">N° Commande</th>
            <th scope="col">Nom</th>
            <th scope="col">Prenom</th>
            <th scope="col">Date</th>
            <th scope="col">Réf Article</th>
            <th scope="col">Quantité</th>
            <th scope="col">Désignation</th>
            <th scope="col">Prix</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($commandes as $commande): ?>
        <tr>
            <th scope="row"><?=$commande['id_comm'];?></th>
            <td><?=$commande['nom'];?></td>
            <td><?=$commande['prenom'];?></td>
            <td><?=$commande['date'];?></td>
            <td><?=$commande['id_article'];?></td>
            <td><?=$commande['quantite'];?></td>
            <td><?=$commande['designation'];?></td>
            <td><?=$commande['prix'];?></td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>