<h1 class="my-4">Connexion</h1>
<form action="/login" method="post">
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control<?=(!empty($errors['email'])) ? " is-invalid" : ""?>" name="email"
            id="email" placeholder="name@example.com" value="<?=(isset($email)) ? $email : ''?>" required>
        <?php if (!empty($errors['email'])) {
    foreach ($errors['email'] as $error) {
        printf('%s %s %s', "<div class=\"invalid-feedback\">", $error, "</div>");
    }
}
?>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Mot de passe</label>
        <input type="password" class="form-control<?=(!empty($errors['password'])) ? " is-invalid" : ""?>"
            name="password" id="password" min="6" required>
        <?php if (!empty($errors['password'])) {
    foreach ($errors['password'] as $error) {
        printf('%s %s %s', "<div class=\"invalid-feedback\">", $error, "</div>");
    }
}
?>
    </div>
    <?php if (!empty($errors['connexion'])) {
    printf('%s %s %s', "<div class=\"my-2 text-danger\">", $errors['connexion'], "</div>");
}
?>
    <button type="submit" class="btn btn-outline-success" value="connexion">
        Connexion
    </button>
</form>

<a href="/signup" class="btn btn-outline-secondary mt-3">
    Inscription
</a>