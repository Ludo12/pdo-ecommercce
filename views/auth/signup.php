<h1 class="my-4">Inscription</h1>
<form action="/signup" method="post" class="mt-4">
    <div class="row">
        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-4">
            <div class="mb-3 row">
                <label for="nom" class="col-form-label" style="width:150px;">Nom</label>
                <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <input type="text" class="form-control<?=(!empty($errors['nom'])) ? " is-invalid" : ""?>" name="nom"
                        id="nom" placeholder="votre nom" value="<?=(isset($nom)) ? $nom : ''?>" min="2" required>

                    <?php if (!empty($errors['nom'])) {
    foreach ($errors['nom'] as $error) {
        printf('%s %s %s', "<div id=\"errorNom\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">2 caractères minimum. Obligatoire.</div>");
}
?>
                </div>
            </div>
        </div>
        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-4">
            <div class="mb-3 row">
                <label for="prenom" class="col-form-label" style="width:150px;">Prenom</label>
                <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <input type="text" class="form-control<?=(!empty($errors['prenom'])) ? " is-invalid" : ""?>"
                        name="prenom" id="prenom" placeholder="votre prenom"
                        value="<?=(isset($prenom)) ? $prenom : ''?>" min="2" required>
                    <?php if (!empty($errors['prenom'])) {
    foreach ($errors['prenom'] as $error) {
        printf('%s %s %s', "<div id=\"errorPrenom\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">2 caractères minimum. Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xxl-4 col-xl-4 col-lg-5 col-md-12 col-sm-4">
            <div class="mb-3 row">
                <label for="adresse" class="col-form-label" style="width:150px;">Adresse</label>
                <div class="col-sm-6 col-md-6 col-lg-7 col-xl-7">
                    <input type="text" class="form-control<?=(!empty($errors['adresse'])) ? " is-invalid" : ""?>"
                        name="adresse" id="adresse" placeholder="4 place du Louvre"
                        value="<?=(isset($adresse)) ? $adresse : ''?>" min="2" required>
                    <?php if (!empty($errors['adresse'])) {
    foreach ($errors['adresse'] as $error) {
        printf('%s %s %s', "<div id=\"errorAdresse\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">2 caractères minimum. Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-4">
            <div class="mb-3 row">
                <label for="code_postal" class="col-form-label" style="width:150px;">Code
                    postal</label>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-7">
                    <input type="text" class="form-control<?=(!empty($errors['code_postal'])) ? " is-invalid" : ""?>"
                        name="code_postal" id="code_postal" placeholder="75001"
                        value="<?=(isset($code_postal)) ? $code_postal : ''?>" min="5" max="5" required>
                    <?php if (!empty($errors['code_postal'])) {
    foreach ($errors['code_postal'] as $error) {
        printf('%s %s %s', "<div id=\"errorCode_postal\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">5 caractères minimum. Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
        <div class="col-xxl-4 col-xl-4 col-lg-3 col-md-5 col-sm-4">
            <div class="mb-3 row">
                <label for="ville" class="col-form-label" style="width:90px;">Ville</label>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-7">
                    <input type="text" class="form-control<?=(!empty($errors['ville'])) ? " is-invalid" : ""?>"
                        name="ville" id="ville" placeholder="Paris" value="<?=(isset($ville)) ? $ville : ''?>" min="2"
                        required>
                    <?php if (!empty($errors['ville'])) {
    foreach ($errors['ville'] as $error) {
        printf('%s %s %s', "<div id=\"errorVille\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">2 caractères minimum. Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-4">
            <div class="mb-3 row">
                <label for="email" class="col-form-label" style="width:150px;">Email</label>
                <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <input type="email" class="form-control<?=(!empty($errors['email'])) ? " is-invalid" : ""?>"
                        name="email" id="email" placeholder="name@example.com"
                        value="<?=(isset($email)) ? $email : ''?>" required>
                    <?php if (!empty($errors['email'])) {
    foreach ($errors['email'] as $error) {
        printf('%s %s %s', "<div id=\"errorEmail\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-4">
            <div class="mb-3 row">
                <label for="password" class="col-form-label" style="width:150px;">Mot de passe</label>
                <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <input type="password" class="form-control<?=(!empty($errors['password'])) ? " is-invalid" : ""?>"
                        name="password" id="password" min="6" required>
                    <?php if (!empty($errors['password'])) {
    foreach ($errors['password'] as $error) {
        printf('%s %s %s', "<div id=\"errorPassword\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">6 caractères minimum. Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
        <div class="col-xxl-4 col-xl-4 col-lg-6 col-md-6 col-sm-4">
            <div class="mb-3 row">
                <label for="password2" class="col-form-label" style="width:150px;">Confirmation</label>
                <div class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <input type="password" class="form-control<?=(!empty($errors['password2'])) ? " is-invalid" : ""?>"
                        name="password2" id="password2" min="6" required>
                    <?php if (!empty($errors['password2'])) {
    foreach ($errors['password2'] as $error) {
        printf('%s %s %s', "<div id=\"errorPassword2\" class=\"invalid-feedback\">", $error, "</div>");
    }
} else {
    printf("%s", "<div id=\"nomHelp\" class=\"form-text\">6 caractères minimum. Obligatoire.</div>");
}?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-outline-success me" value="connexion">
        Inscription
    </button>
</form>