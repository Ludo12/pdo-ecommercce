<h1>Espace Client</h1>
<p><?=$msg;?></p>
<h1>Toutes les commandes</h1>
<?php
foreach ($commandes as $commande):
?>
<div class="card  my-3" style="max-width: 540px;">
    <div class="row g-0">
        <div class="col-md-4 my-auto text-center">
            <img src="<?=$commande['image'];?>" class="img-fluid rounded-start" alt="...">
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">Référence : <?=$commande['id_article'];?></h5>
                <p class="card-text"><?=$commande['designation'];?></p>
                <p class="card-text">Prix Unitaire : <?=$commande['prix'];?> €</p>
                <p class="card-text">Quantité : <?=$commande['quantite'];?></p>
                <p class="card-text">Montant total : <?=(float) $commande['prix'] * (int) $commande['quantite'];?> €</p>
                <p class="card-text"><small class="text-muted">Commande effectuée le :
                        <?=$commande['date'];?></small></p>
            </div>
        </div>
    </div>
</div>
<?php
endforeach;
?>