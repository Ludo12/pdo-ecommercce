<h1>Tous les articles</h1>
<section>
    <div>
        <?=$form?>
    </div>
    <div <?php if (isset($_SESSION['search'])): ?>>
        <form method="GET" action="" class="row g-3 align-items-center my-1">
            <div class="col-auto form-check">
                <button type="submit" class="border border-white bg-transparent">
                    <input type="radio" name="order" class="form-check-input" id="designation" value="designation"
                        <?=($checkDesign) ? "checked" : ''?>>
                    <label for="designation" class="form-check-label">designation</label>
                </button>
            </div>
            <div class="col-auto form-check">
                <button type="submit" class="border border-white bg-transparent">
                    <input type="radio" name="order" class="form-check-input" id="prix" value="prix"
                        <?=($checkPrix) ? "checked" : ''?>>
                    <label for="prix" class="form-check-label">prix</label>
                </button>
            </div>
            <?php if (!empty($errors)):
    foreach ($errors as $nomErreur): ?>
            <div class="col-auto text-danger">
                <?php foreach ($nomErreur as $error) {
        printf("%s", $error);
    }
    ?>
            </div>

            <?php endforeach;
endif;?>
        </form>
    </div <?php endif;?>>
</section>
<section>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Réf</th>
                <th scope="col">Designation</th>
                <th scope="col">Prix</th>
                <th scope="col">Categorie</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php
foreach ($articles as $article):
?>
            <tr>
                <td class="align-middle" scope="row"><?=$article->getId_article()?></td>
                <td class="d-flex align-items-center justify-content-between">
                    <?=$article->getDesignation()?>
                    <a href="articles/<?=$article->getId_article()?>" class="btn btn-info mx-1">Voir plus</a>
                </td>
                <td class="align-middle"><?=$article->getPrix()?> €</td>
                <td class="align-middle"><?=$article->getCategorie()?></td>
                <td class="align-middle">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                        data-bs-target="#popupForm<?=$article->getId_article()?>">
                        Selectionner
                    </button>
                </td>
                <div class="modal fade" id="popupForm<?=$article->getId_article()?>" tabindex="-1"
                    aria-labelledby="popupFormLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="popupFormLabel">Votre article :
                                    <?=$article->getDesignation()?></h5>
                            </div>
                            <form action="/stock" method="post">
                                <div class="modal-body">
                                    <input type="text" class="form-control-plaintext" name="article"
                                        value="Réf : <?=$article->getId_article()?>">
                                    <p>Prix : <?=$article->getPrix()?> €</p>
                                    <span><input type="number" style="width: 50px;" id="quantity" name="quantity"
                                            min="1" max="5" value="1"></span>
                                </div>
                                <div class="modal-footer">

                                    <button type="submit" name="ajouter" class="btn btn-secondary" value="ajouter">
                                        ajouter au panier
                                    </button>

                                    <button type="submit" name="panier" class="btn btn-warning" value="panier">
                                        voir mon panier
                                    </button>
                            </form>
                        </div>
                    </div>
                </div>
                </div>

            </tr>
            <?php
endforeach;
?>
        </tbody>
    </table>
</section>