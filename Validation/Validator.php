<?php
namespace App\Validation;

class Validator
{
    private $donnee;
    private $errors;

    public function __construct(array $donnee)
    {
        $this->donnee = $donnee;
    }

    public function validate(array $rules): ?array
    {
        foreach ($rules as $name => $rulesArray) {
            if (array_key_exists($name, $this->donnee)) {
                foreach ($rulesArray as $rule) {
                    if (key($rulesArray) === 'checked') {
                        $this->checked($name, strip_tags($this->donnee[$name]), $rule);
                    } elseif (key($rulesArray) === 'limite') {
                        if (substr($rule[0], 0, 3) === 'min') {
                            $this->minInt($name, (int) strip_tags($this->donnee[$name]), $rule[0]);}
                        if (substr($rule[1], 0, 3) === 'max') {
                            $this->maxInt($name, (int) strip_tags($this->donnee[$name]), $rule[1]);}
                    } else {
                        switch ($rule) {
                            case 'required':
                                $this->required($name, strip_tags($this->donnee[$name]));
                                break;
                            case substr($rule, 0, 3) === 'min':
                                $this->min($name, strip_tags($this->donnee[$name]), $rule);
                                break;

                            case 'email':
                                $this->email($name, strip_tags($this->donnee[$name]));
                                break;

                            case 'password2':
                                $this->password2($name, $this->donnee['password'], $this->donnee['password2']);
                                break;
                            default:
                                # code...
                                break;
                        }
                    }
                }
            }
        }
        return $this->getErrors();
    }

    private function required(string $name, string $value)
    {
        $value = trim($value);
        if (!isset($value) || is_null($value) || empty($value)) {
            $this->errors[$name][] = "{$name} est requis.";
        }
    }

    private function checked(string $name, string $value, array $order)
    {
        $value = trim($value);
        if (!isset($value) || is_null($value) || empty($value)) {
            $this->errors[$name][] = "{$name} est requis.";
        };

        if (!in_array($value, $order)) {
            $this->errors[$name][] = "L'ordre de tri: {$name}, n'est pas correctement définie.";
        };

    }

    private function min(string $name, string $value, string $rule)
    {
        preg_match_all('/(\d+)/', $rule, $matches);
        $limit = (int) $matches[0][0];

        if (strlen($value) < $limit) {
            $this->errors[$name][] = "{$name} doit avoir un minimun de {$limit} caractères.";
        }
    }

    private function minInt(string $name, int $value, string $rule)
    {
        preg_match_all('/(\d+)/', $rule, $matches);
        $limit = (int) $matches[0][0];

        if ($value < $limit) {
            $this->errors[$name][] = "{$name} ne peut pas être plus petit que {$limit}.";
        }
    }

    private function maxInt(string $name, int $value, string $rule)
    {
        preg_match_all('/(\d+)/', $rule, $matches);
        $limit = (int) $matches[0][0];

        if ($value > $limit) {
            $this->errors[$name][] = "{$name} ne peut pas être plus grand que {$limit}.";
        }
    }

    private function email(string $name, string $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->errors[$name][] = "{$name}  invalid.";
        }
    }

    private function password2(string $name, string $password, string $password2)
    {
        $password = filter_var($password, FILTER_SANITIZE_STRING);
        $password2 = filter_var($password2, FILTER_SANITIZE_STRING);
        if ($password !== $password2) {
            $this->errors[$name][] = "{$name}  invalid.";
        }
    }

    /**
     * Get the value of errors
     */
    private function getErrors(): ?array
    {
        return $this->errors;
    }
}