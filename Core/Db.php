<?php
namespace App\Core;

define('DB_HOST', 'localhost');

# the username used to access DB
define('DB_USER', '****');

# the password for the username
define('DB_PASS', '****');

# the name of your databse
define('DB_NAME', 'pme');

use PDO;
use PDOException;

class Db extends PDO
{
    private static $conn;

    private function __construct($username = DB_USER, $password = DB_PASS, $host = DB_HOST, $dbname = DB_NAME, $options = [])
    {
        try {
            parent::__construct("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options);
            $this->setAttribute(parent::ATTR_ERRMODE, parent::ERRMODE_EXCEPTION);
            $this->setAttribute(parent::ATTR_DEFAULT_FETCH_MODE, parent::FETCH_OBJ);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }
    public static function run()
    {
        if (self::$conn === null) {
            self::$conn = new self();
        }
        return self::$conn;
    }

    public static function disconnect()
    {
        self::$conn = null;
    }
}
